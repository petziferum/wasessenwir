export default [
  {
    id: 1,
    name: "Kalter Fisch",
    description: "Einfach kalter Fisch, ohne Beilage. Lecker!!",
    ingredients: ["Fisch", "Salz", "Wasser"],
    level: "Easy"
  },
  {
    id: 2,
    name: "Pizza",
    description: "Pizza mit Schinken!!",
    ingredients: ["Tomatensoße", "Schinken", "Käse"],
    level: "Easy"
  },
  {
    id: 3,
    name: "Lasagne",
    description: "Lasagne Bolognese",
    ingredients: ["Tomatensoße", "Hackfleisch", "Käse", "Schmand"],
    level: "Easy"
  },
  {
    id: 4,
    name: "Phò",
    description: "Phò mit Fleisch und viiieeel Hoysin",
    ingredients: ["Phó", "Rindfleisch", "Hoysin"],
    level: "Easy"
  },
  {
    id: 5,
    name: "Schnitzel",
    description: "PutenSchnitzel",
    ingredients: ["Putenfilet", "Ei", "Semmelbrösel", "Mehl", "Öl"],
    level: "Easy"
  }
];
